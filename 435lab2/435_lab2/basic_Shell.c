/*
 * basic_Shell.c
 *
 *  Created on: 2014-07-28
 *      Author: Adrien Lapointe
 */


//testing 123
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const unsigned int cmd_max_len = 1024;
const unsigned int max_num_arg = 64;



void welcome_prompt();
void parse(char *input, char **argv);
void type_prompt(char *input);
void interpret_command(char **argv);

int main() {

	char input[cmd_max_len];             // the input
	char *argv[max_num_arg];              // the arguments

	int pid, status;

	welcome_prompt();

	while (1) {

		type_prompt(input);
		pid = waitpid(-1, &status, WNOHANG); // This is necessary for the fork.
		parse(input, argv);
		interpret_command(argv);

	}

	return 0;
}

/*
 * This functions prints the prompt message and accepts input from the user.
 */
void type_prompt(char *input) {

	printf("GEF435$");

	if (input != NULL) {
		int c = EOF;
		int i = 0;

		// take in input until user hits enter or end of file is encountered.
		while ((c = getchar()) != '\n' && c != EOF) {
			input[i++] = (char) c;
		}

		input[i] = '\0';
	}
}

/*
 * This function parses the user inputs.
 */
void parse(char *input, char **argv) {

	// This is where the code for parsing the user's input is.
	// C'est l'endroit ou vous placez le code pour separer
	// les entree de l'utilisateur.
}

/*
 * This function interprets the parsed command that is entered by the user and
 * calls the appropriate built-in function or calls the appropriate program.
 */
void interpret_command(char **argv) {
	// This is where you will write code to call the appropriate function or program.
	// L'endroit ou vous appelerez les fonctions ou programmes appropries.
}

/*
 * This function prints the welcome message.
 */
void welcome_prompt() {
	int num_Padding = 41;
	int i;

// Prints the first line of padding.
	for (i = 0; i < num_Padding; i++)
		printf("#");
	printf("\n#\tWelcome to the GEF435 shell\t#\n");
	for (i = 0; i < num_Padding; i++)
		printf("#");
	printf("\n\n");
}
